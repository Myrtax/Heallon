﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class InterfaceMain : MonoBehaviour {

	public Text ballonsVermelhosText;
	public Text metragemText;
	public GameObject aviso;
	public bool avisoPerm;
	public GameObject barraMetragem;
	public GameObject[] vidas;
	// Use this for initialization
	void Start () 
	{

	}
	
	// Update is called once per frame
	void Update () 
	{
		ballonsVermelhosText.text = ": "+Central.ballonsVermelhos;
		barraMetragem.transform.localScale = new Vector3(1f,Central.altura/1000,1f);
		metragemText.text = "H:"+ Mathf.RoundToInt( Central.altura)+"m";
		if (Mathf.RoundToInt( Central.altura) % 100 == 0) 
		{
			if (avisoPerm)
			{
				aviso.GetComponent<Text>().text = Mathf.RoundToInt( Central.altura)+"m";
				aviso.GetComponent<Animator>().SetBool("Ativar",true);
				avisoPerm = false;
			}
			else
			{
				aviso.GetComponent<Animator>().SetBool("Ativar",false);
			}
		}
		else
		{
			avisoPerm = true;
		}
		for (int i = 0; i < vidas.Length; i++) 
		{
			if (i < Player.vida) 
			{
				vidas[i].SetActive(true);
			}
			else
			{
				vidas[i].SetActive(false);
			}
		}
	}
}
