﻿using UnityEngine;
using System.Collections;

public class Obstaculo : MonoBehaviour {

	public string tipo;
	// Use this for initialization
	void Start () 
	{
	
	}
	
	// Update is called once per frame
	void Update () 
	{
	
	}

	void OnTriggerEnter2D(Collider2D colider)
	{
		if (colider.gameObject.tag == "Player") 
		{
			switch (tipo) 
			{
				case "Raio":
					Player.vida--;
					GerenciadorEfeitos.efeito = "Choque";
					GerenciadorEfeitos.ativo = true;
					Destroy(gameObject);
					break;
				case "Chuva":
					GerenciadorEfeitos.efeito = "Chuva";
					GerenciadorEfeitos.ativo = true;
					break;
				case "Neve":
					GerenciadorEfeitos.efeito = "Neve";
					GerenciadorEfeitos.ativo = true;
					break;
				case "Flu":
					Player.vida--;
					Destroy(gameObject);
					break;
				default:
				break;
			}
		}
	}
}
