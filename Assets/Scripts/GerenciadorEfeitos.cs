﻿using UnityEngine;
using System.Collections;

public class GerenciadorEfeitos : MonoBehaviour {

	public Efeito[] efeitos;
	public static bool zerar;
	public static bool ativo;
	public static string efeito;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () 
	{
		if (ativo)
		{
			for (int i = 0; i < efeitos.Length; i++) 
			{
				if (efeitos[i].tipo == efeito) 
				{
					efeitos[i].ativo = true;
					ativo = false;
				}
			}
		}
		if (zerar)
		{
			Zerar();
			zerar = false;
		}
	}

	void Zerar()
	{
		for (int i = 0; i < efeitos.Length; i++) 
		{
			efeitos[i].ativo = false;
		}
	}
}
