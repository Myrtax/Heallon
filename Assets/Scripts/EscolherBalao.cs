﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class EscolherBalao : MonoBehaviour {

	public Sprite[] baloes;
	public static Sprite[] baloesPublico;
	public Image balaoImage;
	public GameObject cadeado;
	public int indice;
	public int[] indicesBloqueados;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () 
	{
		baloesPublico = baloes;
		for (int i = 0; i < indicesBloqueados.Length; i++) 
		{
			if (indice == indicesBloqueados[i]) 
			{
				cadeado.SetActive(true);
				i = indicesBloqueados.Length;
			}
			else
			{
				cadeado.SetActive(false);
			}
		}
		balaoImage.sprite = baloes [indice];
		if (!cadeado.activeSelf) 
		{
			PlayerPrefs.SetInt ("indiceBalao", indice);
		}
	}

	public void editarIndice(int value)
	{
		indice += value;
		if (indice < 0) 
		{
			indice = baloes.Length-1;
		}
		if (indice == baloes.Length) 
		{
			indice = 0;
		}
	}
}
