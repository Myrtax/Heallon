﻿using UnityEngine;
using System.Collections;

public class Click : MonoBehaviour {


	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnMouseDown()
	{
		if (this.gameObject.name == "BtnLeft") 
		{
			Player.esquerda = true;
		}
		if (this.gameObject.name == "BtnRight") 
		{
			Player.direita = true;
		}
	}

	void OnMouseUp()
	{
		if (this.gameObject.name == "BtnLeft") 
		{
			Player.esquerda = false;
		}
		if (this.gameObject.name == "BtnRight") 
		{
			Player.direita = false;
		}
	}

	public void MudarCena(string cena)
	{
		Application.LoadLevel(cena);
	}

	public void Sair()
	{
		Application.Quit ();
	}

	public static void MudarCenaCode(string cena)
	{
		Application.LoadLevel(cena);
	}

}
