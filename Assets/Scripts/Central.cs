﻿using UnityEngine;
using System.Collections;

public class Central : MonoBehaviour {

	public static int ballonsVermelhos;
	public static float altura;
	// Use this for initialization
	void Start () 
	{

	}
	
	// Update is called once per frame
	void Update () 
	{
		altura += 2 * Time.deltaTime;
	}

	public void salvar()
	{
		PlayerPrefs.SetInt("ballonsVermelhos", ballonsVermelhos);
	}

	public void carregar()
	{
		ballonsVermelhos = PlayerPrefs.GetInt("ballonsVermelhos");
	}

	public void recomeçar()
	{
		Player.vida = 3;
		GerenciadorEfeitos.zerar = true;
		Player.velocidade = 3;
		Player.esquerda = false;
		Player.direita = false;
	}
}
