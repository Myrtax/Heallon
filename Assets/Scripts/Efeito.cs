﻿using UnityEngine;
using System.Collections;

public class Efeito : MonoBehaviour {

	public bool ativo;
	public string tipo;
	public float cont;
	// Use this for initialization
	void Start () 
	{
	
	}
	
	// Update is called once per frame
	void Update () 
	{
		if (ativo) 
		{
			switch (tipo) 
			{
				case "Choque":
					Choque();
					break;
				case "Chuva":
					Chuva();
					break;
				case "Neve":
					Neve();
					break;
				default:
					break;
			}
		}
	}

	void Choque()
	{
		if (cont >= 0.3f) 
		{
			ativo = false;
			cont = 0;
		}
		else
		{
			cont += Time.deltaTime;
		}
		GetComponent<Animator> ().SetBool ("Ativo", ativo);
	}

	void Chuva()
	{
		if (cont >= 4f) 
		{
			ativo = false;
			cont = 0;
			Player.velocidade = 3;
		}
		else
		{
			Player.velocidade = 1.5f;
			cont += Time.deltaTime;
		}
	}

	void Neve()
	{
		if (cont >= 4f) 
		{
			ativo = false;
			cont = 0;
			Player.velocidade = 3;
		}
		else
		{
			Player.velocidade = 0.5f;
			cont += Time.deltaTime;
		}
	}
}
