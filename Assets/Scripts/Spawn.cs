﻿using UnityEngine;
using System.Collections;

public class Spawn : MonoBehaviour {

	public GameObject prefabAtual;
	public GameObject[] prefabs;
	public float time, timeL;
	public Transform PointMin, PointMax;
	// Use this for initialization
	void Start () 
	{
	
	}
	
	// Update is called once per frame
	void Update () 
	{
		if (time >= timeL) 
		{
			prefabAtual = prefabs[Mathf.RoundToInt(Random.Range(0,prefabs.Length))];
			Instantiate(prefabAtual, new Vector3(Random.Range(PointMin.position.x,PointMax.position.x),Random.Range(PointMin.position.y,PointMax.position.y),0f),Quaternion.identity);
			time = 0;
		}
		time += Time.deltaTime;
	}
}
