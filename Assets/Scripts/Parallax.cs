﻿using UnityEngine;
using System.Collections;

public class Parallax : MonoBehaviour {

	public Transform posicaoFinal;
	public Transform pointMinInicial, pointMaxInicial;
	public float velocidadeX, velocidadeY;
	// Use this for initialization
	void Start () 
	{
	
	}
	
	// Update is called once per frame
	void Update () 
	{
		transform.position += new Vector3 (velocidadeX *Time.deltaTime, velocidadeY*Time.deltaTime, 0f);
		if (transform.position.y <= posicaoFinal.position.y) 
		{
			transform.position = new Vector3(Random.Range(pointMinInicial.position.x,pointMaxInicial.position.x),Random.Range(pointMinInicial.position.y,pointMaxInicial.position.y),transform.position.z);
		}
	}
	
}
