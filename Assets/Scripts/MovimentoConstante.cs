﻿using UnityEngine;
using System.Collections;

public class MovimentoConstante : MonoBehaviour {

	public float velocidadeX;
	public float velocidadeY;
	public string tipo;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () 
	{
		if (tipo == "X") 
		{
			transform.position += new Vector3 (velocidadeX * Time.deltaTime,0f, 0f);
		}
		if (tipo == "Y") 
		{
			transform.position += new Vector3 (0f, velocidadeY * Time.deltaTime, 0f);
		}
		if (tipo == "XY") 
		{
			transform.position += new Vector3 (velocidadeX * Time.deltaTime, velocidadeY * Time.deltaTime, 0f);
		}
	}
}
