﻿using UnityEngine;
using System.Collections;

public class Coletavel : MonoBehaviour {

	public int value;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnTriggerEnter2D(Collider2D colider)
	{
		if (colider.gameObject.tag == "Player") 
		{
			addBallonVermelho();
			Destroy(gameObject);
		}
	}

	void addBallonVermelho()
	{
		Central.ballonsVermelhos += value;
	}
}
