﻿using UnityEngine;
using System.Collections;

public class Player : MonoBehaviour {

	public static float velocidade = 3;
	public static bool esquerda,direita;
	public static float vida = 3, vidaL = 3;
	// Use this for initialization
	void Start () 
	{
		GetComponent<SpriteRenderer> ().sprite = EscolherBalao.baloesPublico [PlayerPrefs.GetInt ("indiceBalao")];
	}
	
	// Update is called once per frame
	void Update () 
	{
		if (esquerda || Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.LeftArrow))  
		{
			Andar(-1);
		}
		if (direita || Input.GetKey(KeyCode.D) || Input.GetKey(KeyCode.RightArrow)) 
		{
			Andar(1);
		}
		if (vida <= 0) 
		{
			Click.MudarCenaCode("GameOver");
		}
	}

	void Andar(float direcao)
	{
		transform.position += new Vector3 (velocidade * Time.deltaTime * direcao, 0f, 0f);
	}
}
